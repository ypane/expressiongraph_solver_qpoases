#include <expressiongraph/qpoases_solver.hpp>
#include <kdl/expressiontree.hpp>
#include <expressiongraph/qpoases_messages.hpp>
#include <iostream>

namespace KDL {

qpOASESSolver::qpOASESSolver(int _nWSR, double _cputime,double _regularisation_factor ):
    regularisation_factor(_regularisation_factor),nWSR(_nWSR),cputime(_cputime) {
}

int qpOASESSolver::setupCommon(Context::Ptr _ctx, int isRuntime) {
    ctx           = _ctx;
    ndx_time      = ctx->getScalarNdx("time");
    // construct index of all variables: nv_robot, nv_feature and the time variables (i.e. the state) :
    all_ndx.clear();
    ctx->getScalarsOfType("robot",all_ndx);  
    nv_robot      = all_ndx.size();
    ctx->getScalarsOfType("feature",all_ndx);  
    nv_feature    = all_ndx.size() - nv_robot;
    all_ndx.push_back(ndx_time);
    
    if (!isRuntime) {
      // allocate and initialize state:
      state.resize(all_ndx.size());
      for (size_t i=0;i<all_ndx.size();++i) {
	  VariableScalar* vs = ctx->getScalarStruct(all_ndx[i]);
	  state[i] = vs->initial_value;
      }
    }
    else
    {
      // allocate, BUT no state initialization anymore
//       std::cout << "state vector size: " << state.size() << std::endl;
// 	    for (unsigned int i=0; i<state.size(); ++i) 
// 	      std::cout << state[i] << " ";
// 	    std::cout << std::endl;      
      double timestate = state[state.size()-1];
      int sizediff = all_ndx.size()-state.size();
      state.conservativeResize(all_ndx.size());
      if (sizediff > 0) 
      {
          state.conservativeResize(all_ndx.size());
          state[state.size()-1] = timestate;
          
          for (int i=1; i<=sizediff; ++i)
          {
              state[state.size()-1-i] = 0.0;
          }
      }
//       std::cout << "state vector size after resizing: " << state.size() << std::endl; 
// 	    for (unsigned int i=0; i<state.size(); ++i) 
// 	      std::cout << state[i] << " ";
// 	    std::cout << std::endl;           
    }      
    
    // add all expressions to the optimizer (context + output specification + monitors):
    expr_opt.prepare(all_ndx); 
    ctx->addToOptimizer(expr_opt);

    // count and check the number of constraints for the different priority levels:
    nc_priority.resize(3);
    for (size_t i=0;i<nc_priority.size();++i) {
        nc_priority[i]=0;
    }
    for (size_t indx=0;indx< ctx->cnstr_scalar.size();++indx) {
        ConstraintScalar& c = ctx->cnstr_scalar[indx];
        if (c.active) {
            if (c.priority >= (int)nc_priority.size()) {
                return -1;
            }
            nc_priority[ c.priority ] += 1;
        }
    }
    return 0;
}



void qpOASESSolver::reset() {
    H         = Eigen::MatrixXd::Identity(nv,nv)*regularisation_factor;
    A         = Eigen::MatrixXd::Zero(nc,nv);
    lb        = Eigen::VectorXd::Constant(nv,-HUGE_VALUE);
    lbA       = Eigen::VectorXd::Constant(nc,-HUGE_VALUE);
    ub        = Eigen::VectorXd::Constant(nv,HUGE_VALUE);
    ubA       = Eigen::VectorXd::Constant(nc,HUGE_VALUE);
    g         = Eigen::VectorXd::Zero(nv);
}
void qpOASESSolver::setupMatrices(int _nv, int _nc) {
    nv        = _nv;
    nc        = _nc;
    reset();
    firsttime = true;
    if (nv!=0) {
        QP        = qpOASES::SQProblem(nv,nc,qpOASES::HST_POSDEF);
    }
    QP.setPrintLevel(qpOASES::PL_NONE);
    solution.resize(nv);
}

void qpOASESSolver::printMatrices(std::ostream& os) {
    os << "H:\n" << H << "\n";
    os << "lb:\n" << lb.transpose() << "\n";
    os << "ub:\n" << ub.transpose() << "\n";
    os << "A:\n" << A << "\n";
    os << "lbA:\n" << lbA.transpose() << "\n";
    os << "ubA:\n" << ubA.transpose() << std::endl; 
    os << "solution:\n" << solution.transpose() << std::endl; 
    os << "state:\n" << state.transpose() << std::endl; 
}

int qpOASESSolver::prepareExecution(Context::Ptr ctx) {
    int retval = setupCommon(ctx, 0);
    if (retval!=0) return retval;
    initialization = false;
    // set up x variable as : robot | feature | slack
    optim_ndx.clear();
    ctx->getScalarsOfType("robot",optim_ndx);  
    ctx->getScalarsOfType("feature",optim_ndx);  
    setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]  );
    
    wa_transient = 1.0;
    wb_transient = 0.0;
    return 0; 
}

int qpOASESSolver::prepareExecutionRunTime(Context::Ptr ctx) {
    // initialize variables
    wa_transient = 1.0;
    wb_transient = 0.0;
    transient_Tf = 0.5;
    transient_Ti = 0.25;
    t_scale      = 1.0/transient_Ti;
    transient_time = 0.0;
    
    
    // Algorithm:
    // 1. Check the newly added skill priority
    // 2. If the new skill has priority lower or equal to the lowest priority level,
    //    then proceed (no transient)
    // 3. Otherwise, assign the newly added skill to the lowest priority level
    // 4. Perform transient weight 
    // 4. After transient is finished, increase the priority level
    // 6. Repeat 4-5 until the skill's original priority level is reached
    
    // Step 3
    if (ctx->transient_flag) {
       ctx->current_runtime_operation = Context::ADDITION;
       ctx->current_transient_phase = Context::PRE_PRIORITY_SWITCH;
       std::string str = "global.";
       for (std::map<std::string,int>::iterator skill=ctx->transient_skills.begin(); skill!=ctx->transient_skills.end(); ++skill) { 
          str.append(skill->first); // "global.skill_name"
          for (size_t indx=0;indx<ctx->cnstr_scalar.size();++indx) {
             ConstraintScalar& c = ctx->cnstr_scalar[indx];
             if (c.groupname == str) {
                // temporarily assign the newly added constraints to lowest priority level
                c.priority = ctx->lowest_priority_level;
                skill->second = ctx->lowest_priority_level;
             }
          }
          str = "global.";
       }
    }
    
    // get the time when transient starts
    transient_t0 = state[nv_robot+nv_feature];
//     begin = std::chrono::steady_clock::now();
//     std::cout << "transient_t0: " << transient_t0 << std::endl;

    int retval = setupCommon(ctx, 1);
    if (retval!=0) return retval;
    initialization = false;
    // set up x variable as : robot | feature | slack
    optim_ndx.clear();
    ctx->getScalarsOfType("robot",optim_ndx);  
    ctx->getScalarsOfType("feature",optim_ndx); 
//     std::cout << "nc_priority: " << nc_priority[0] << "  " << nc_priority[1] << "   " << nc_priority[2] << std::endl;
    setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]  );
   
    return 0; 
}

int qpOASESSolver::removeSkillRunTime(Context::Ptr ctx) {
    // initialize variables
    wa_transient = 1.0;
    wb_transient = 0.0;
    transient_Tf = 0.5;
    transient_Ti = 0.25; // TODO: these timing related parameters should be configurable
    t_scale      = 1.0/transient_Ti;
    transient_time = 0.0;
    
    if (ctx->transient_flag) { // Transient skill removal
       ctx->current_runtime_operation = Context::REMOVAL;
       ctx->current_transient_phase = Context::PRE_PRIORITY_SWITCH;
       
       // Update the transient skills priority
          std::map<std::string, int>::iterator it;
          it = ctx->priority_map.find(ctx->skill_to_remove);
          
          if (it != ctx->priority_map.end()) {
              ctx->transient_skills[it->first] = it->second; // the transient skill always starts from its original priority level
          }
    } else { 
       // Non-transient skill removal
       deleteSkill();
        
       // resize the QP matrices
       setupCommon(ctx, true);
       setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]  );      
    }    
    // get the time when transient starts
    transient_t0 = state[nv_robot+nv_feature];
    ctx->activate_transient_weighting = true;  

//     std::cout << "finish executing removeSkillRunTime" << std::endl;
//     
//     // print priority_map just to check
//     for (std::map<std::string,int>::iterator it=ctx->priority_map.begin(); it!=ctx->priority_map.end(); ++it) {
//         std::cout << "skill: " << it->first << std::endl;
//     }    
    return 0;
}

int qpOASESSolver::computeTransientInitialTime() {
    // get the time when transient starts
    transient_t0 = state[nv_robot+nv_feature];
}

void qpOASESSolver::deleteSkill() {
    std::map<std::string, int>::iterator it;
    it = ctx->priority_map.find(ctx->skill_to_remove);
              
    // check scalar constraint
    std::string str;
    for (auto iter = ctx->cnstr_scalar.cbegin(); iter != ctx->cnstr_scalar.cend(); ) {
        str = iter->name;
        if (str.find(ctx->skill_to_remove) != -1) {
           ctx->cnstr_scalar.erase(iter);
        } 
        else {
           ++iter;
        }                  
    }
    
    // check box constraint
    for (auto iter = ctx->cnstr_box.cbegin(); iter != ctx->cnstr_box.cend(); ) {
        str = iter->name;
        if (str.find(ctx->skill_to_remove) != -1) {
           ctx->cnstr_box.erase(iter);
        }
        else {
           ++iter;
        }                  
    }
              
    // remove skill from priority map
    if (it != ctx->priority_map.end()) {
        ctx->priority_map.erase(it);
    }    
}

void qpOASESSolver::deleteMonitors(const std::string& skillname) {
    std::string str;
    for (auto iter = ctx->mon_scalar.cbegin(); iter != ctx->mon_scalar.cend(); ) {
        str = iter->groupname;
        if (str.find(skillname) != -1) {
           ctx->mon_scalar.erase(iter);
        } 
        else {
           ++iter;
        }                  
    }      
}

int qpOASESSolver::prepareInitialization(Context::Ptr ctx) {
    priority_switch_check = false;
    int retval = setupCommon(ctx, 0);
    if (retval!=0) return retval;
    initialization = true;
    // set up x variable as : feature 
    optim_ndx.clear();
    ctx->getScalarsOfType("feature",optim_ndx);  
    setupMatrices( nv_feature + nc_priority[0],   nc_priority[0] );
    return 0;
}


void qpOASESSolver::fillHessian() {
    // valid for both execution and initialization
    // optim_ndx contains the variable indices of the variables involved in the optimization (not including slack variables)
    for (size_t i = 0; i < optim_ndx.size(); ++i ) {
        VariableScalar* vs = ctx->getScalarStruct( optim_ndx[i] );
        H(i,i)=vs->weight->value()*regularisation_factor;
    }
}

void qpOASESSolver::fillConstraintScalar(const ConstraintScalar& c) {
    if (initialization && c.priority!=0) return;
    assert( (0<=cnr)&&(cnr<nc));

    //called for updating the model
    double modelval  = c.model->value();
    double modelder  = c.model->derivative(ndx_time); 
    double measval   = c.meas->value();
    if (c.target_lower==-HUGE_VALUE) {
        lbA(cnr)        = -HUGE_VALUE;
    } else {
        lbA(cnr)        = c.controller_lower->compute(c.target_lower-measval, -modelder);
    }
    if (c.target_upper==HUGE_VALUE) {
        ubA(cnr)        = HUGE_VALUE;
    } else {
        ubA(cnr)        = c.controller_upper->compute(c.target_upper-measval, -modelder);
    }
    
    for (size_t i=0;i<optim_ndx.size();++i) {
        A(cnr,i) = c.model->derivative(optim_ndx[i]);
    }
    
//     std::cout << "wa_transient: " << wa_transient << "   wb_transient: " << wb_transient << std::endl;
    if ((c.priority==2) || (initialization && (c.priority==0))) {
        // soft constraint: additional slack var:
        // assert( (0<=softcnr)&&(softcnr<nc_priority[2]));   not correct when initializing
        int j = optim_ndx.size() + softcnr;
        A(cnr,j) = 1.0;

        
        if (ctx->transient_flag) {
           // Check whether the constraint belongs to the transient phase
           std::map<std::string, int>::iterator it;
           std::string str = c.groupname;
           it = ctx->transient_skills.find(str.erase(0,7));
           
           if (ctx->current_runtime_operation == Context::ADDITION) {  
               if (ctx->current_transient_phase == Context::PRE_PRIORITY_SWITCH) {
                   if (it != ctx->transient_skills.end()) {
                       H(j,j)   = regularisation_factor + c.weight->value()*wb_transient; 
                   }
                   else {
                       H(j,j)   = regularisation_factor + c.weight->value()*wa_transient;
                   }
               }
               else if (ctx->current_transient_phase == Context::POST_PRIORITY_SWITCH) {
                   if (it != ctx->transient_skills.end()) {
                       H(j,j)   = regularisation_factor + c.weight->value(); 
                   }
                   else {
                       H(j,j)   = regularisation_factor + c.weight->value()*wa_transient;
                   }
               }
           }
           else if (ctx->current_runtime_operation == Context::REMOVAL) {
               if (ctx->current_transient_phase == Context::PRE_PRIORITY_SWITCH) {
                   // Only change (decrease) the weights of to-be-kept skills/or one priority lower skills
                   if (it != ctx->transient_skills.end()) {
                       H(j,j)   = regularisation_factor + c.weight->value(); 
                   }
                   else {
                       H(j,j)   = regularisation_factor + c.weight->value()*wa_transient;
                   }
               }
               else if (ctx->current_transient_phase == Context::POST_PRIORITY_SWITCH) {
                   // Apply decreasing weights to the to-be-removed skills
                   if (it != ctx->transient_skills.end()) {
                       H(j,j) = regularisation_factor + c.weight->value()*wb_transient;
                   }
                   // Apply increasing weights to the skills at the same (last) priority level
                   else {
                       H(j,j) = regularisation_factor + c.weight->value()*wa_transient;
                   }                                                                 
               }
           }
        } 
        else {
           H(j,j)   = regularisation_factor + c.weight->value();
        }
        softcnr++; 
     }
     cnr++;
     
}

void qpOASESSolver::fillConstraintBox(const ConstraintBox& c) {
    for (size_t i=0;i<optim_ndx.size();++i) {
        if (optim_ndx[i]==c.variablenr) {
            lb[i] = c.target_lower;
            ub[i] = c.target_upper;
        }
    }
}

void qpOASESSolver::fill_constraints() {
    reset();
    softcnr = 0;
    cnr     = 0;
    for (size_t indx=0;indx<ctx->cnstr_scalar.size();++indx) {
        ConstraintScalar& c = ctx->cnstr_scalar[indx];
        if (c.active) {
            fillConstraintScalar(c);
        }
    }
    for (size_t indx=0;indx<ctx->cnstr_box.size();++indx) {
        ConstraintBox& c = ctx->cnstr_box[indx];
        if (c.active) {
            fillConstraintBox(c);
        }
    }
}
    
void qpOASESSolver::setPrintLevel(int n) {
    QP.setPrintLevel((qpOASES::PrintLevel)n);
}

int qpOASESSolver::solve() {
    
    /*  Weights calculation based on the transient phases */
    if ((ctx->transient_flag) && (ctx->activate_transient_weighting)) {
         // calculate elapsed transient time
         transient_time = state[nv_robot+nv_feature] - transient_t0;
//          std::cout << "transient_time: " << transient_time  << std::endl;
//          std::cout << "wa_transient: " << wa_transient << "   wb_transient: " << wb_transient << std::endl;

        // check if transient has finished
        if (transient_time >= transient_Tf)  {
//             std::cout << "3 transient_time: " << transient_time  << std::endl;
//             end = std::chrono::steady_clock::now();
//             std::cout << "Time difference = " << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() << "[µs]" << "   Transient time: " << transient_time <<  "   transient_Tf: " << transient_Tf  << "   time now: " << state[nv_robot+nv_feature] << "   transient_t0: " << transient_t0 <<std::endl;
            
            if (ctx->current_runtime_operation == Context::REMOVAL) {
                // remove the skill
                deleteSkill();
                
                // resize the QP matrices
                setupCommon(ctx, true);
                setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]);     
            }
            
           
           // reset priority switch flag
           priority_switch_check = false;
           
           ctx->current_runtime_operation = Context::NONE; // reset the runtime operation 
           transient_time = 0.0;
           
           // reassign the weights
           wa_transient = 1.0;
           wb_transient = 0.0;
           
           // clear the transient skill map
           ctx->transient_skills.clear();

           // set the transient flag to false
           ctx->transient_flag = false;    
           ctx->activate_transient_weighting = false;
           std::cout << "-----------!!!!!!!!!! setting transient flag to FALSE ********" << std::endl;
            
        } else if (transient_time < transient_Ti) { // pre-priority switch phase
//            std::cout << "1 transient_time: " << transient_time  << std::endl;
           // calculate the transient weights
           transient_time_scaled = transient_time*t_scale;
           wb_transient = transient_time_scaled - (0.15915)*sin(6.28318*transient_time_scaled); // tau - (1/(2*pi))*sin(2*pi*tau)
           wa_transient = std::max(0.0, 1-wb_transient);            
    
        } else if ((transient_time >= transient_Ti) && (transient_time < transient_Tf)) { // during and post-priority switch phase
//            std::cout << "2 transient_time: " << transient_time  << std::endl;            
           // increment/decrement the priority level of the transient skills
           if (!priority_switch_check) {
               std::cout << "****** Performing priority switch check ********" << std::endl;
               if (ctx->current_runtime_operation == Context::ADDITION) {
                   auto it = ctx->transient_skills.begin();
                   auto it_des = ctx->priority_map.find(it->first);
                   std::cout << "Transient skills  " << it->first << std::endl;
        
                   // If the transient skill priority is not the same as the desired priority, then switch
                   if (it->second != it_des->second) {                   
                       std::string str;
                       for (size_t indx=0;indx<ctx->cnstr_scalar.size();++indx) {
                          ConstraintScalar& c = ctx->cnstr_scalar[indx];
                          
                          // find the constraints that belong to the transient phase
                          std::map<std::string, int>::iterator it;
                          str = c.groupname;
                          std::cout << "Runtime addition str: " << str << std::endl;
                          it = ctx->transient_skills.find(str.erase(0,7));
                
                          if (it != ctx->transient_skills.end()) {
                              if (c.priority == 2) {
                                c.priority = 1; // increase the priority level
                                std::cout << "priority level is increased " << std::endl;
                              }
                          }
                       }
                       // resize the QP matrices
                       setupCommon(ctx, true);
                       setupMatrices( nv_robot+nv_feature+nc_priority[2],   nc_priority[0]+nc_priority[1]+nc_priority[2]  );
                   } else {
                       std::cout << "target priority is already the same as the current priority, no need to change" << std::endl;
                   }
            
               } else if (ctx->current_runtime_operation == Context::REMOVAL) {
                   std::cout << "when applicable, the priority level should be decreased" << std::endl;
                   
                   std::string str;
                   for (size_t indx=0;indx<ctx->cnstr_scalar.size();++indx) {
                      ConstraintScalar& c = ctx->cnstr_scalar[indx];
                      
                      // find the constraints that belong to the transient phase
                      std::map<std::string, int>::iterator it;
                      str = c.groupname;
                      it = ctx->transient_skills.find(str.erase(0,7));
            
                      if (it != ctx->transient_skills.end()) {
                          if (c.priority == 1)  {
                             c.priority = 2; // decrease the priority level
                             std::cout << "priority level is decreased " << std::endl;
                          }
                      }
                   }               
               }
               
               /* entering the post switch phase, change the time scaling factor */
               t_scale      = 1.0/(transient_Tf-transient_Ti);
               
               priority_switch_check = true;
               
               ctx->current_transient_phase = Context::POST_PRIORITY_SWITCH;
           }
           
           double t = transient_time - transient_Ti;
           transient_time_scaled = t*t_scale;
           wa_transient = transient_time_scaled - (0.15915)*sin(6.28318*transient_time_scaled);
           wb_transient = std::max(0.0, 1-wa_transient);
        }
    }
    /* End of weights calculation based on the transient phases */
        
    // set input values of all expressions
    expr_opt.setInputValues( state );  // without optimizer: ctx->setInputValues(all_ndx,state);
    
    // set up the QP matrices
    if (nv==0) return 0;
    fill_constraints(); 
    fillHessian(); // fill_constraints calls reset() that resets the values of H, so this should follow fill_constraints.
    //printMatrices(std::cout);
    
    // call the qpOASES solver
    int    _nWSR    = nWSR;
    double _cputime = cputime;
    int retval;
    if (firsttime) {
        retval=QP.init (H.data(), g.data(), A.data(), lb.data(), ub.data(), lbA.data(), ubA.data(), _nWSR, &_cputime);
        firsttime=false;
    } else {
        retval=QP.hotstart (H.data(), g.data(), A.data(), lb.data(), ub.data(), lbA.data(), ubA.data(), _nWSR, &_cputime);
    }
    if (retval!=0) return retval;
    retval = QP.getPrimalSolution(solution.data());
    //std::cout << solution.transpose() << std::endl;
    norm_change = 0;
    for (int i=0;i<optim_ndx.size();++i) {
            // do not count the slack variables.
        norm_change += solution[i]*solution[i];
    }
    return retval;
}


double qpOASESSolver::getWeightedResult() {
    //std::cout << solution.transpose() << std::endl;
    double val = 0.0;
    for (int i=optim_ndx.size();i<nv;++i) {
        //std::cout << i << " ===> " << solution[i] << "\t\t" << H(i,i) << std::endl;
        val +=  solution[i]*solution[i]*H(i,i);
    }
    return val;
}




int qpOASESSolver::updateStep(double dt) {
    int retval = solve();
    if (retval!=0) return retval;
    if (initialization) {
        // solution contains feature 
        // state    contains robot | feature | time  but (explicit) time remains constant.
        for (int i=0;i<nv_feature;++i) {
            state[nv_robot+i] += solution[i] * dt;
        } 
    } else {
        // solution contains robot | feature | slack velocities 
        // state    contains robot | feature | time
        for (int i=0;i<nv_robot+nv_feature;++i) {
            state[i] += solution[i] * dt;
        } 
        state[nv_robot+nv_feature] += dt;
    } 
    return 0;
}


std::string qpOASESSolver::errorMessage(int code) {
    int nrOfMessages=0;
    while (qpoases_messages[nrOfMessages]!=0) {
        if (code == nrOfMessages) {
            return qpoases_messages[code];
        }
        nrOfMessages++; 
    }
    std::stringstream ss;
    ss << "UNKNOWN ERRORCODE " << code;
    return ss.str();
}

std::string qpOASESSolver::getName() {
    return "qpOASES_velocity_resolution";
}

/*

// === NOT YET VERIFIED OR TESTED:
void qpOASESSolver::getJointNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_robot;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i] )->name ] = i; 
    }
}
void qpOASESSolver::getJointNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_robot);
    for (int i=0;i<nv_robot;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i] )->name; 
    }
}
void qpOASESSolver::getFeatureNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_feature;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i+nv_robot] )->name ] = i; 
    }
}
void qpOASESSolver::getFeatureNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_feature);
    for (int i=0;i<nv_feature;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i+nv_robot] )->name; 
    }
}



void qpOASESSolver::setFeatureStates(const Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        state[i+nv_robot] = featstate[i];
    }
}
void qpOASESSolver::getFeatureStates(Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        featstate[i] = state[i+nv_robot];
    }
}
void qpOASESSolver::setTime(double time) {
    state[nv_robot+nv_feature] = time;
}

double qpOASESSolver::getTime() {
    return state[nv_robot+nv_feature];
}

void qpOASESSolver::getJointVelocities(Eigen::VectorXd& _jntvel) {
    assert( !initialization );
    for (int i=0;i<nv_robot;++i) {
        _jntvel[i] = solution[i];
    }
}
void qpOASESSolver::getFeatureVelocities(Eigen::VectorXd& _featvel) {
    assert( !initialization );
    for (int i=0;i<nv_feature;++i) {
        _featvel[i] = solution[i+nv_robot];
    }
}
*/



/**
 * if you do not want to solve() but still want to evaluate all the 
 * expressions
 */
void qpOASESSolver::evaluate_expressions() { 
    expr_opt.setInputValues( state );
    solution.setZero(nv);   // fill zero in the solution vector
}


void qpOASESSolver::getJointNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_robot;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i] )->name ] = i;
    }
}

void qpOASESSolver::getJointNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_robot);
    for (int i=0;i<nv_robot;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i] )->name;
    }
}

void qpOASESSolver::getFeatureNameToIndex(std::map<std::string,int>& namemap) {
    namemap.clear();
    for (int i=0;i<nv_feature;++i) {
        namemap[ ctx->getScalarStruct( all_ndx[i+nv_robot] )->name ] = i;
    }
}

void qpOASESSolver::getFeatureNameVector(std::vector<std::string>& namevec) {
    namevec.resize(nv_feature);
    for (int i=0;i<nv_feature;++i) {
        namevec[i] = ctx->getScalarStruct( all_ndx[i+nv_robot] )->name;
    }
}

int qpOASESSolver::getNrOfJointStates() {
    return nv_robot;
}

int qpOASESSolver::getNrOfFeatureStates() {
    return nv_feature;
}

void qpOASESSolver::setJointStates(const Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        state[i] = jntstate[i];
    }
}

void qpOASESSolver::getJointStates(Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        jntstate[i] = state[i];
    }
}

void qpOASESSolver::setAndUpdateJointStates(const Eigen::VectorXd& jntstate) {
    assert( jntstate.size() == nv_robot );
    for (int i=0;i<nv_robot;++i) {
        state[i] = jntstate[i];
    }
    expr_opt.setInputValues( state );
}

void qpOASESSolver::setFeatureStates(const Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        state[i+nv_robot] = featstate[i];
    }
}

void qpOASESSolver::getFeatureStates(Eigen::VectorXd& featstate) {
    assert( featstate.size() == nv_feature );
    for (int i=0;i<nv_feature;++i) {
        featstate[i] = state[i+nv_robot];
    }
}

void qpOASESSolver::setTime( double time) {
    state[nv_robot+nv_feature] = time;
}

double qpOASESSolver::getTime(){
    return state[nv_robot+nv_feature];
}

void qpOASESSolver::getJointVelocities(Eigen::VectorXd& _jntvel){
    assert( !initialization );
    for (int i=0;i<nv_robot;++i) {
        _jntvel[i] = solution[i];
    }
}

void qpOASESSolver::getFeatureVelocities(Eigen::VectorXd& _featvel){
    assert( !initialization );
    for (int i=0;i<nv_feature;++i) {
        _featvel[i] = solution[i+nv_robot];
    }
}

double qpOASESSolver::getNormChange(){
    //assert( initialization );
    return sqrt(norm_change);
}

void qpOASESSolver::setState(const Eigen::VectorXd& _state) {  
    state = _state;
}

void qpOASESSolver::getState(Eigen::VectorXd& _state) {
    _state = state;
}


void qpOASESSolver::setInitialValues(){
     for (size_t i=0;i<all_ndx.size();++i) {
         VariableScalar* vs = ctx->getScalarStruct(all_ndx[i]);
         vs->initial_value = state[i];
     }
}




}// end of namespace KDL
